package it.unimi.di.sweng.lab02;

public class BowlingGame implements Bowling {
	private int[] rolls=new int[21];
	private int currentRoll=0;
	
	@Override
	public void roll(int pins) {
		
		rolls[currentRoll++]=pins;
	}

	@Override
	public int score() {
		int tot=0;
		for(int currentRoll=0;currentRoll<rolls.length;currentRoll++){
			if(isStrike(currentRoll))
				tot+=rolls[currentRoll+1]+rolls[currentRoll+2];
			else if(isSpare(currentRoll))
				tot+=rolls[currentRoll+2];
			tot+=rolls[currentRoll];
		}
		return tot;
	}
	private boolean isSpare(int currentRoll){
		return currentRoll%2==0 && currentRoll<rolls.length-1 && rolls[currentRoll]+rolls[currentRoll+1] == 10 
				&&currentRoll<18;
	}
	private boolean isStrike(int currentRoll){
		
		return (rolls[currentRoll]==10 && currentRoll == 0) || (rolls[currentRoll]==10 
				&& currentRoll>0 && currentRoll<18 && rolls[currentRoll-1]!=0);
	}
}
